package ex_list_package;

public class Carta {
	// attributes
	public String pal;
	public String valor;
	
	// constructor
	public Carta (String pal, String valor) {
		this.pal = pal;
		this.valor = valor;
	}
	// to string
	@Override
	public String toString() {
		return "Carta [pal=" + pal + ", valor=" + valor + "]";
	}
}
