package ex_list_package;

import java.util.LinkedList;
import java.util.ListIterator;

public class Ex_2 {
	// attributes
	LinkedList<Integer> numList2;
	
	
	
	
	protected Ex_2() {
		this.numList2 = new LinkedList<Integer>();
	}

	// methods
	
	//add num
	public void myAddList(LinkedList<Integer> l) {
		numList2.addAll(l);
	}
	
	// add new elements to list 2
	public void myAdd(String n) {
		numList2.add(Integer.parseInt(n));
	}
	
	// print list
	public void printList() {
		System.out.println("Recorregut llista cap en davant:");
		ListIterator<Integer> listIt = numList2.listIterator();
		while(listIt.hasNext()) {
			System.out.println(listIt.next());
		}
	}
	
	// check for l1 in l2
	public boolean containsList(LinkedList<Integer> l2) {
			return numList2.containsAll(l2);
		
	}
	public boolean containsEls(LinkedList<Integer> ls) {
		ListIterator<Integer> listIt = ls.listIterator();
		boolean isContained = numList2.contains(listIt.next());
		while(listIt.hasNext() && isContained) {
			isContained = numList2.contains(listIt.next());
		}
		if (isContained) {
			numList2.clear();
			return true;
		}
		return false;
	}
}





















