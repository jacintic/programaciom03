package ex_list_package;

import java.util.ArrayList;
import java.util.Collections;
import java.util.ListIterator;

public class Ex_5 {
	// attributes
	ArrayList<Persona2> persones;
	
	
	
	
	protected Ex_5() {
		this.persones = new ArrayList<Persona2>();
	}

	// methods
	
	// add new elements to list 2
	public void myAdd(Persona2 p) {
		persones.add(p);
	}
	
	// build a list with only minors, return a list
	public ArrayList<Persona2> menorsEdat() { 
		ArrayList<Persona2> result = new ArrayList<Persona2>();
		ListIterator<Persona2> listIt = persones.listIterator();
		while(listIt.hasNext()) {
			Persona2 currPers = listIt.next();
			if (currPers.age >= 18) {
				break;
			}
			result.add(currPers);
		}
		return result;
	}

	// return  stringbuilder element with all the elements of a list
	public StringBuilder printList(ArrayList<Persona2> p) {
		StringBuilder result = new StringBuilder("");  
		ListIterator<Persona2> listIt = p.listIterator();
		while(listIt.hasNext()) {
			result.append(listIt.next());  
			result.append("\n");
		}
		return result;
	}
	
		
	// sort method
	public void mySort() {
		Collections.sort(persones);
	}
	
	
	
}





















