package ex_list_package;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

public class Ex_6 {
	// attributes
	List<Persona3> persones;
	
	
	
	
	protected Ex_6() {
		this.persones = new ArrayList<Persona3>();
	}

	// methods
	
	// add new elements to list 2
	public void myAdd(Persona3 p) {
		persones.add(p);
	}
	
	// build a list with only minors, return a list
	public ArrayList<Persona3> menorsEdat() { 
		ArrayList<Persona3> result = new ArrayList<Persona3>();
		ListIterator<Persona3> listIt = persones.listIterator();
		while(listIt.hasNext()) {
			Persona3 currPers = listIt.next();
			if (currPers.age >= 18) {
				break;
			}
			result.add(currPers);
		}
		return result;
	}

	// return  stringbuilder element with all the elements of a list
	public StringBuilder printList(List<Persona3> p) {
		StringBuilder result = new StringBuilder("");  
		ListIterator<Persona3> listIt = p.listIterator();
		while(listIt.hasNext()) {
			result.append(listIt.next());  
			result.append("\n");
		}
		return result;
	}
	
		
	// sort method
	public void mySort() {
		Collections.sort(persones);
	}
	
	
	
}





















