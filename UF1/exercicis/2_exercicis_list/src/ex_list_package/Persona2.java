package ex_list_package;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Persona2 implements Comparable<Persona2> {
	// attributes
	public static int id = 1;
	public int dni;
	public String name;
	public String surname;
	public int age;
	public LocalDateTime birthDate;
	
	// constructor
	public Persona2(String name, int age, String surname, LocalDateTime birthDate) {
		this.dni= Persona2.id;
		Persona2.id++;
		this.name = name;
		this.age = age;
		this.surname = surname;
		this.birthDate = birthDate;
	}
	@Override
	public String toString() {
		// data formatter
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		String formatDateTime = birthDate.format(formatter);
		return "Persona [name=" + name + ", surname=" + surname + ", age=" + age + ", dni=" + dni + ", birthDate=" + formatDateTime + "]";
	}
	// compareTo
	@Override
    public int compareTo(Persona2 persona) { 
        
        if (this.birthDate.isBefore(persona.birthDate)) return -1;
        else if (this.birthDate.equals(persona.birthDate)) {
        	return this.surname.compareTo(persona.surname);
        }
        else return 1;
    }
}





















