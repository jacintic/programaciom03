package ex_list_package;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Persona3 implements Comparable<Persona3> {
	// attributes
	public static int id = 1;
	public int dni;
	public String name;
	public String surname;
	public int age;
	public LocalDateTime birthDate;
	
	// constructor
	public Persona3(String name, int age, String surname, LocalDateTime birthDate) {
		this.dni= Persona3.id;
		Persona3.id++;
		this.name = name;
		this.age = age;
		this.surname = surname;
		this.birthDate = birthDate;
	}
	
	@Override
	public String toString() {
		// data formatter
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		String formatDateTime = birthDate.format(formatter);
		return "Persona [name=" + name + ", surname=" + surname + ", age=" + age + ", dni=" + dni + ", birthDate=" + formatDateTime + "]";
	}
	// compareTo
	@Override
    public int compareTo(Persona3 persona) { 
        if (this.surname.compareTo(persona.surname) < 0) return -1;
        else if (this.surname.compareTo(persona.surname) == 0) {
        	return this.name.compareTo(persona.name);
        }
        else return 1;
    }
}





















