package ex_list_package;

import java.util.List;

public class Player {
	// attributes
	public int id;
	public List<Carta> myCardsPl;
	
	// constructor
	public Player (int id, List<Carta> myCardsPl) {
		this.id = id;
		this.myCardsPl = myCardsPl;
	}

	@Override
	public String toString() {
		return "Player [id=" + id + ", myCards=" + myCardsPl + "] TotalCards: " + myCardsPl.size() ;
	}
	
}
