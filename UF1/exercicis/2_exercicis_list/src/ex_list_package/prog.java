package ex_list_package;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

public class prog {

	public static void main(String[] args) {
		
		System.out.println("---------Exercici 1--------");
		System.out.println();
		////////////////////////
		////// Exercici 1///////
		////////////////////////
		// initialize
		Ex_1 myList = new Ex_1();
		
		// add items
		myList.myAdd("1");
		myList.myAdd("300");
		myList.myAdd("99");
		
		System.out.println("Print added objects");
		// print item
		myList.printList();
		
		System.out.println("purge items greater than 100");
		// purge greater than 100
		myList.purgeGtT100();
		
		// print item
		myList.printList();
		
		// add more items
		myList.myAdd("400");
		myList.myAdd("500");
		
		System.out.println("sort list");
		// sort items
		myList.mySort();
		
		// print list
		myList.printList();
		
		////////////////////////
		////// Exercici 2///////
		////////////////////////
		System.out.println("---------Exercici 2--------");
		System.out.println();
		
		// initialize
		Ex_2 myList2 = new Ex_2();
		
		// adding numbers
		System.out.println("Adding numbers");
		myList2.myAdd("37");
		myList2.myAdd("342");
		myList2.myAdd("15");
		
		System.out.println("Printing list");
		// print numbers on new list
		myList2.printList();
		
		System.out.println("Appending list 1 to list 2");
		// add the whole list to this one (append)
		myList2.myAddList(myList.retList());
		
		System.out.println("Printing list");
		// print numbers on new list
		myList2.printList();
		
		System.out.println();
		// check for list 2 containing all elements of list 1
		System.out.println("Does list 2 contain all elements from list1? " + myList2.containsEls(myList.retList()));
		
		System.out.println("Printing list");
		// print numbers on new list
		myList2.printList();
		
		////////////////////////
		////// Exercici 3///////
		////////////////////////
		System.out.println("---------Exercici 3--------");
		System.out.println();
		
		System.out.println("Generating persons");
		// create persons
		Persona p1 = new Persona("Manolo", 16);
		Persona p2 = new Persona("Francesc", 15);
		Persona p3 = new Persona("Cisco", 18);
		Persona p4 = new Persona("Paco", 22);
		Persona p5 = new Persona("Pakito", 23);
		System.out.println();
		System.out.println("Adding Persons to list in Ex_3");
		Ex_3 myPersons = new Ex_3();
		myPersons.myAdd(p1);
		myPersons.myAdd(p2);
		myPersons.myAdd(p3);
		myPersons.myAdd(p4);
		myPersons.myAdd(p5);
		System.out.println();
		System.out.println("Printing all persons");
		System.out.println(myPersons.printList(myPersons.persones));
		System.out.println();
		System.out.println("Obtaining list of minors");
		System.out.println(myPersons.printList(myPersons.menorsEdat()));
		
		////////////////////////
		////// Exercici 4///////
		////////////////////////
		System.out.println("---------Exercici 4--------");
		System.out.println();
		
		System.out.println("Generating persons");
		// create persons
		Persona p21 = new Persona("Manolo", 16);
		Persona p22 = new Persona("Francesc", 15);
		Persona p23 = new Persona("Cisco", 18);
		Persona p24 = new Persona("Paco", 22);
		Persona p25 = new Persona("Pakito", 23);
		System.out.println();
		System.out.println("Adding Persons to list in Ex_4");
		Ex_4 myPersons2 = new Ex_4();
		myPersons2.myAdd(p21);
		myPersons2.myAdd(p22);
		myPersons2.myAdd(p23);
		myPersons2.myAdd(p24);
		myPersons2.myAdd(p25);
		System.out.println();
		System.out.println("Printing all persons");
		System.out.println(myPersons2.printList(myPersons2.persones));
		System.out.println();
		// here comes the sort!
		System.out.println("Sorting all persons by age (ascending)");
		myPersons2.mySort();
		System.out.println(myPersons2.printList(myPersons2.persones));
		System.out.println();
		System.out.println("Obtaining list of minors");
		System.out.println(myPersons2.printList(myPersons2.menorsEdat()));
		
		////////////////////////
		////// Exercici 5///////
		////////////////////////
		System.out.println("---------Exercici 5--------");
		System.out.println();
		
		System.out.println("Generating persons");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		// create persons
		Persona2 p31 = new Persona2("Manolo", 16, "Perez", LocalDateTime.parse("15-08-1954 00:01", formatter));
		Persona2 p32 = new Persona2("Francesc", 15, "Gimenez",LocalDateTime.parse("15-08-1954 00:01", formatter));
		Persona2 p33 = new Persona2("Cisco", 18, "Vermudez", LocalDateTime.parse("15-08-1955 00:01", formatter));
		Persona2 p34 = new Persona2("Paco", 22, "Lopez", LocalDateTime.parse("15-08-1956 00:01", formatter));
		Persona2 p35 = new Persona2("Pakito", 23, "Juarez",LocalDateTime.parse("15-08-1958 00:01", formatter));
		System.out.println();
		System.out.println("Adding Persons to list in Ex_5");
		Ex_5 myPersons3 = new Ex_5();
		myPersons3.myAdd(p31);
		myPersons3.myAdd(p32);
		myPersons3.myAdd(p33);
		myPersons3.myAdd(p34);
		myPersons3.myAdd(p35);
		System.out.println();
		System.out.println("Printing all persons");
		System.out.println(myPersons3.printList(myPersons3.persones));
		System.out.println();
		// here comes the sort!
		System.out.println("Sorting all persons by age (ascending)");
		myPersons3.mySort();
		System.out.println(myPersons3.printList(myPersons3.persones));
		System.out.println();
		
		
		////////////////////////
		////// Exercici 6///////
		////////////////////////
		System.out.println("---------Exercici 6--------");
		System.out.println();
		
		System.out.println("Generating persons");
		// create persons
		Persona3 p41 = new Persona3("Manolo", 16, "Perez", LocalDateTime.parse("15-08-1954 00:01", formatter));
		Persona3 p42 = new Persona3("Francesc", 15, "Perez",LocalDateTime.parse("15-08-1954 00:01", formatter));
		Persona3 p43 = new Persona3("Cisco", 18, "Vermudez", LocalDateTime.parse("15-08-1955 00:01", formatter));
		Persona3 p44 = new Persona3("Paco", 22, "Blasio", LocalDateTime.parse("15-08-1956 00:01", formatter));
		Persona3 p45 = new Persona3("Pakito", 23, "Abelardo",LocalDateTime.parse("15-08-1958 00:01", formatter));
		Persona3 p46 = new Persona3("Pakito", 24, "Aabelardo",LocalDateTime.parse("15-08-1958 00:01", formatter));
		System.out.println();
		System.out.println("Adding Persons to list in Ex_6");
		Ex_6 myPersons4 = new Ex_6();
		myPersons4.myAdd(p41);
		myPersons4.myAdd(p42);
		myPersons4.myAdd(p43);
		myPersons4.myAdd(p44);
		myPersons4.myAdd(p45);
		myPersons4.myAdd(p46);
		System.out.println();
		System.out.println("Printing all persons");
		System.out.println(myPersons4.printList(myPersons4.persones));
		System.out.println();
		// here comes the sort!
		System.out.println("Sorting all persons by age (ascending)");
		myPersons4.mySort();
		System.out.println(myPersons4.printList(myPersons4.persones));
		System.out.println();
		
		
		////////////////////////
		////// Exercici 7///////
		////////////////////////
		System.out.println("---------Exercici 7--------");
		System.out.println();
		
		System.out.println("Generating cards");
		// Initialise baralla
		Baralla myCards = new Baralla();
		// fill cards stack
		myCards.crearBaralla();
		// printing card list
		System.out.println("Printing cards");
		System.out.println(myCards.printList());
		// shuffling cards
		System.out.println("Shuffling cards");
		myCards.myShuffle();
		System.out.println("Printing cards");
		System.out.println(myCards.printList());
		System.out.println("Total cards: " + myCards.baralla.size());
		// adding players
		System.out.println("Initializing players");
		myCards.initializePlayers(5);
		System.out.println(myCards.printPlayers());		
	}

}
