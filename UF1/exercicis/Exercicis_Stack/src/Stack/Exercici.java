package Stack;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;

public class Exercici {

	// print dequeue
	public static Deque printDeque(Deque adq, String phrase) {
		// imprimim missatge
		System.out.println(phrase);
		// recorrem pila i imprimim els elements
		Iterator<Integer> iterator = adq.iterator();
		while (iterator.hasNext()) {
			Integer element = iterator.next();
			System.out.println(element);
		}
		return adq;
	}

	// print dequeue (generic)
	public static <T> void printDequeGen(Deque<T> adq) {
		// imprimim missatge
		// recorrem pila i imprimim els elements
		Iterator<T> iterator = adq.iterator();
		while (iterator.hasNext()) {
			Object element = iterator.next();
			System.out.println(element);
		}
	}
	// invert dequeue

	public static Deque<Integer> invertirPila(Deque<Integer> stack) {
		Deque<Integer> inversStack = new ArrayDeque<Integer>();
		while (!stack.isEmpty()) {
			inversStack.push(stack.pop());

		}
		printDeque(inversStack, "Deque (pila) invertida amb mètode <Integer>:");
		return inversStack;

	}

	/// EXERCICI 2
	// mostra de la mina
	// String[] mine1 = {"<","<","(",")",">",">"};
	/*
	 * +>
	 * *<
	 * + -<
	 * 
	 * +>
	 * *<
	 * +-<
	 * 
	 * 
	 * */
	//String[] mine1 = {"<","<","(",")",">",">"};
	/* No entenc com compta els diamants negres, el que jo entenc es que cuan ha comptat 2 blancs a la possició 4 i 5 de l'arrray normal
	 * ja para de comptar pero aquest programa sembla que segueix comptant no se com*/

	/*
	 * <
	 * <<
	 * <<(
	 * <<()
	 * ()
	 * */
	 ////////////////// pop primero de la pila (ultimo elemento añadida)
	 /////////////////// peekFirst primero de la pila (ultimo elemento añadido)
	public static void extractDiamonds(String[] mine) {
		Deque<String> pilaDiamants = new ArrayDeque<String>();
		int blanc = 0;
		int negre = 0;
		for (String elem : mine) {
			System.out.println("estat pila : " + pilaDiamants);
			if (!pilaDiamants.isEmpty() && 
					pilaDiamants.peekFirst().equals("<") && 
					elem.equals(">")) {
				System.out.println("Elements: " +  elem);
				System.out.println("before delete: " + pilaDiamants.peekFirst());
				blanc++;
				pilaDiamants.pop();
				System.out.println("after delete: " + pilaDiamants.peekFirst());
			} else {
				if (!pilaDiamants.isEmpty() && 
					pilaDiamants.peekFirst().equals("(") && 
					elem.equals(")")) {
					negre++;
					System.out.println("Elements: " +  elem);
					System.out.println("before delete: " + pilaDiamants.peekFirst());
					pilaDiamants.pop();
					System.out.println("after delete: " + pilaDiamants.peekFirst());
				} else {
					System.out.println("Elements: " +  elem);
					System.out.println("before push: " + pilaDiamants.peekFirst());
					pilaDiamants.push(elem);
					System.out.println("after push: " + pilaDiamants.peekFirst());
				}
			}
		}
		System.out.println("A la mina hi ha " + blanc + " diamants blancs i " + negre + " diamants negres.");
	}
	
	/////// FET AMB STRINGS , FUNCIONA ////////
	/*
	public static void extractDiamonds(Deque<String> mine) {
		// keeps track of the number of the element to be printed later
		int counter = 0;
		// element iterator
		Iterator<String> myIt = mine.iterator();
		while (myIt.hasNext()) {
			int whiteDimes = 0;
			int blackDimes = 0;

			String element = myIt.next();
			// get white diamonds inside black or white diamonds
			System.out.println("This batch of diamonds: [" + element + "]");
			String pattern1 = ".*<<>>.*|.*\\(<>\\).*";
			while (element.matches(pattern1)) {
				if (element.matches(".*<<>>.*|.*\\(<>\\).*")) {
					whiteDimes++;
				}
				if (element.matches(".*<<>>.*")) {
					element = element.replaceFirst("(.*)(<)<>(>)(.*)", "$1$2$3$4");
				} else if (element.matches(".*(\\()<>(\\)).*")) {
					element = element.replaceFirst("(.*)(\\()<>(\\))(.*)", "$1$2$3$4");
				}

			}
			// pick single white diamonds left
			String pattern2 = ".*<>.*";
			while (element.matches(pattern2)) {
				element = element.replaceFirst("(.*)<>(.*)", "$1$2");
				whiteDimes++;
			}
			// pick black diamonds with a < between (<) => ()
			String pattern3 = ".*\\(<\\).*";
			while (element.matches(pattern3)) {
				element = element.replaceFirst("(.*)\\(<\\)(.*)", "$1$2");
				blackDimes++;
			}
			// pick () black diamonds left
			String pattern4 = ".*\\(\\).*";
			while (element.matches(pattern4)) {
				element = element.replaceFirst("(.*)\\(\\)(.*)", "$1$2");
				blackDimes++;
			}
			System.out.println("What's left of the diamond batch after processing: [" + element + "]");
			System.out.println("There were " + whiteDimes + " white diamonds and " + blackDimes
					+ " black diamonds in elment " + counter);
			System.out.println();
			counter++;
		}

	}
	*/
	

	// EXERCICI 3
	public static void checkParentheses(String op) {
		// element iterator
		Deque<String> cua = new ArrayDeque<String>();
		Deque<String> pila = new ArrayDeque<String>();
		// fill cua of parentheses
		for (int i = 0; i < op.length(); i++) {
			if (op.charAt(i) == '(' || op.charAt(i) == ')') {
				cua.add("" + op.charAt(i));
			}
		}
		// print current state of "cua"
		System.out.println("Cua:");
		System.out.println(cua);
		// check parentheses correctness method
		Iterator<String> iterator = cua.iterator();
		while (iterator.hasNext()) {
			String element = iterator.next();
			// add "("
			if (element.equals("(")) {
				pila.add(element);
			// remove one element if element is ")" and "pila" isn't empty
			} else if (element.equals(")") && pila.size() > 0) {
				pila.pop();
			// if "pila" is empty and current element is ")" add to "pila"
			} else if (element.equals(")") && pila.size() == 0) {
				pila.add(")");
			}
		}
		// print final state of pila
		System.out.println(pila);
		// evaluate correct status
		if (pila.size() == 0) {
			System.out.println("El format es correcte.");
		} else {
			System.out.println("El format es incorrece.");
		}
	}

	// EXERCICI 4
	public static void reverseIterator(Deque<String> que) {
		System.out.println("---- Reversing Deque with iterator ----");
		Deque<String> queCopy = new ArrayDeque<String>();
		Iterator<String> iterator = que.iterator();
		while (iterator.hasNext()) {
			String element = iterator.next();
			queCopy.addFirst(element);
		}
		printDequeGen(queCopy);
		System.out.println();
	}

	public static void reverseNoIt(Deque<String> que) {
		System.out.println("---- Reversing Deque without iterator ----");
		Deque<String> inversStack = new ArrayDeque<String>();
		while (!que.isEmpty()) {
			inversStack.push(que.pop());

		}
		printDequeGen(inversStack);
	}
	public static <T> void reverseIteratorGen(Deque<T> que) {
		System.out.println("---- Reversing Deque with iterator (Generic) ----");
		Deque<T> queCopy = new ArrayDeque<T>();
		Iterator<T> iterator = que.iterator();
		while (iterator.hasNext()) {
			Object element = iterator.next();
			queCopy.addFirst((T) element);
		}
		printDequeGen(queCopy);
		System.out.println();
	}
	
	public static <T> void reverseNoItGen(Deque<T> que) {
		System.out.println("---- Reversing Deque without iterator (Generic) ----");
		Deque<T> inversStack = new ArrayDeque<T>();
		while (!que.isEmpty()) {
			inversStack.push(que.pop());

		}
		printDequeGen(inversStack);
	}
}
