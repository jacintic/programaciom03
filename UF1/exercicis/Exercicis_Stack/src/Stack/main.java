package Stack;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		///// EXERCICI 1 ///////
		
		System.out.println("---- Exercici 1 ----");
		Deque<Integer> stackDeque = new ArrayDeque<Integer>();
		stackDeque.add(1);
		stackDeque.add(2);
		stackDeque.add(3);
		stackDeque.add(4);
		stackDeque.add(5);
		System.out.println("Print Queue");
		Exercici.printDeque(stackDeque, "");
		System.out.println("Flip Queue");
		stackDeque = Exercici.invertirPila(stackDeque);
		Exercici.printDeque(stackDeque, "resultat final");
		System.out.println();

		
		///// EXERCICI 2 ///////
		
		System.out.println("---- Exercici 2 ----");
		/*
		 * DESCOMENTAR PER EVALUAR
		System.out.println("Filling mine:");
		Deque<String> pilaDiamants = new ArrayDeque<String>();
		pilaDiamants.add("");
		pilaDiamants.add("<>");
		pilaDiamants.add("()");
		pilaDiamants.add("<<>>");
		pilaDiamants.add("<><<>><(<)");
		pilaDiamants.add("<>(<>)<(<)");
		System.out.println("----------------------------------------------------------");
		System.out.println("ATTENTION: I've decided to count '(<)' as a black diamond.'");
		System.out.println("----------------------------------------------------------");
		Exercici.extractDiamonds(pilaDiamants);
		System.out.println();
		*/
		System.out.println("Filling mine:");
		String[] mine1 = {"<","<","(",")",">",">"};
		Exercici.extractDiamonds(mine1);
		///// EXERCICI 3 ///////
		
		System.out.println("---- Exercici 3 ----");
		String op1 = "25+3*(1+2+(30*4))/2"; // correcte
		String op2 = "25+3*(1+2+(30*4))/(2"; // erroni
		String op3 = "25+3*(1+2+(30*4))/)2("; // erroni
		String op4 = "25+3*(1+2)+(30*4))/2"; // erroni
		Exercici.checkParentheses(op1);
		Exercici.checkParentheses(op2);
		Exercici.checkParentheses(op3);
		Exercici.checkParentheses(op4);
		

		///// EXERCICI 4 ///////
		System.out.println("---- Exercici 4 ----");
		Deque<String> cua1 = new LinkedList<String>();
		Deque<String> cua2 = new ArrayDeque<>();
		cua1.addAll(Arrays.asList("1","2","3","4"));
        cua2.addAll(Arrays.asList("1","2","3","4"));
        System.out.println("Printing original order in cua1:");
        Exercici.printDequeGen(cua1);
        System.out.println("Printing original order in cua2:");
        Exercici.printDequeGen(cua2);
        Exercici.reverseIterator(cua1);
        Exercici.reverseIterator(cua2);
        Exercici.reverseNoIt(cua1);
        Exercici.reverseNoIt(cua2);
        System.out.println();
        //////////  AMB GENERICS  ////////// 
        System.out.println(" ---- Utilitzant generics ----");
        cua1.addAll(Arrays.asList("1","2","3","4"));
        cua2.addAll(Arrays.asList("1","2","3","4"));
        Exercici.printDequeGen(cua2);
        Exercici.reverseIteratorGen(cua1);
        Exercici.reverseIteratorGen(cua2);
        Exercici.reverseNoItGen(cua1);
        Exercici.reverseNoItGen(cua2);

	}

}
