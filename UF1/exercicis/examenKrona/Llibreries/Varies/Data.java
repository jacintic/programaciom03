package Varies;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import Llibreries.Cadena;

public abstract class Data {
	public static  LocalDateTime formatData(String data) {
		if(data == null) {
			return null;
		}
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		return LocalDateTime.parse(data, formatter);
	}
	// format data examen
	public static  LocalDateTime formatDataDMY(String data) {
		if(data == null) {
			return null;
		}
		data += " 00:00"; 
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		return LocalDateTime.parse(data, formatter);
	}
	public static  String formatDataLDT(LocalDateTime data) {
		if(data == null) {
			return "NULL";
		}
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		return data.format(formatter);
	}
	public static boolean esData(String dataTmp) {
		String[] dataComponents = dataTmp.split("-");
		if (dataComponents.length != 3) {return false;}
		for (int i = 0; i < dataComponents.length; i++) {
			if (Cadena.stringIsInt(dataComponents[i]) == false) {
				return false;
			}
		}
		return true;
	}
}
