package Llibreries;

public class Cadena {
	// method to check if string is int
	public static boolean stringIsInt(String cadena){
		try {
			Integer.parseInt(cadena);
			return true;
		} catch (NumberFormatException nfe){
			return false;
		}
	}
	// method that returns first non string coordinate
	public static String nonnonValidCoord(String cadena){
		try {
			Integer.parseInt(cadena);
			return "";
		} catch (NumberFormatException nfe){
			return cadena;
		}
	}
}
