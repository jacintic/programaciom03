/**
 * 
 */
package inheritance;

/**
 * @author yo
 *
 */
public class Tech extends Employee {

	/**
	 * @param employeeId
	 * @param name
	 * @param age
	 */
	public double hourlyWage;
	
	public Tech(String employeeId, String name, int age, double hourlyWage) {
		super(employeeId, name, age);
		this.hourlyWage = hourlyWage;
	}

	@Override
	public double getSalary(double hoursWorked) {
		
		return hourlyWage * hoursWorked;
	}

}
