package inheritance;

public class main {

	public static void main(String[] args) {
		// constructors
		Design d = new Design("D1", "John Design", 34, 350);
		Tech t = new Tech("T1", "Francis Tech", 42, 45);
		HRdepartment h = new HRdepartment("HR1", "Paul HR", 53, 3500);
		
		// testing methods
		d.getData();
		t.getData();
		h.getData();
		// setting
		d.setData("D2", "John Designeir",35);
		d.getData();
		// salary method
		System.out.println(d.getSalary(4));
		System.out.println(t.getSalary(160));
		System.out.println(h.getSalary(1));
	}

}
