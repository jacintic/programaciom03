package javaCollections;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.PrimitiveIterator.OfDouble;

public class JavaCollections {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		///////////// ARRAYS////////////
		int edades[];

		// 2. Instanciamos el array de int
		edades = new int[3];

		// 3. Inicializamos los valores de los elementos del array
		edades[0] = 30;
		edades[1] = 15;
		edades[2] = 20;

		// 4.imprimimos los valores del array
		System.out.println("array enteros indice 0:" + edades[0]);
		System.out.println("array enteros indice 1:" + edades[1]);
		System.out.println("array enteros indice 2:" + edades[2]);
		// System.out.println("arreglo enteros indice 0:" + edades[3]);

		// 1. Declarar e instanciar un array de tipos object
		Persona personas[] = new Persona[4];
		// 2. Inicializar los valores del array
		personas[0] = new Persona("Juan");
		personas[2] = new Persona("Carla");

		System.out.println("array personas indice 0:" + personas[0]);
		System.out.println("array personas indice 1:" + personas[1]);
		System.out.println("array personas indice 2:" + personas[2]);
		System.out.println("array personas indice 3:" + personas[3]);
		// System.out.println("Array personas indice 4:" + personas[4]);

		///////////// ARRAYLIST///////////
		ArrayList<String> l1 = new ArrayList<String>();
		// add elements
		l1.add("Gat");
		l1.add("Jaguar");
		l1.add("Elefant");
		// access/get elements via index
		System.out.println(l1.get(0));
		System.out.println(l1.get(1));
		// System.out.println(l1.get(3)); // will give Erro: "Index 3 out of bounds for
		// length 3"
		// replace element
		l1.set(1, "Gos");
		// contains
		if (l1.contains("Gos")) {
			System.out.println("Conte Gos");
		}
		// loop
		System.out.println(l1.get(1));
		System.out.println("-------for of loop:-------");
		for (String animal : l1) {
			System.out.println(animal);
		}
		System.out.println("-------While loop:-------");
		int i = 0;
		// loop2
		while (i < l1.size()) {
			System.out.println(l1.get(i));
			i++;
		}
		System.out.println("-------For loop:-------");
		// loop3
		for (int j = 0; j < l1.size(); j++) {
			System.out.println(l1.get(j));
		}
		System.out.println("-------Iterator:-------");
		System.out.println("-------Iterator: => needs to import-------");
		// Iterator
		Iterator<String> iter = l1.iterator();

		// Displaying the values after iterating
		// through the list
		System.out.println("\nThe iterator values" + " of list are: ");
		while (iter.hasNext()) {
			System.out.println(iter.next() + " ");
		}
		System.out.println("-------FOREACH:-------");
		l1.forEach((animal) -> System.out.println(animal));

		System.out.println("-----------Fill in a String array with the elements from the ArrayList");
		String s[] = new String[l1.size()];
		Iterator<String> iter2 = l1.iterator();

		// Displaying the values after iterating
		// through the list
		System.out.println("\nThe iterator values" + " of list are: ");
		int counter = 0;
		while (iter2.hasNext()) {
			s[counter] = iter2.next();
			counter++;
		}
		for (int k = 0; k < s.length; k++) {
			System.out.println(s[k]);
		}
		System.out.println("Contingut array simple: ");
		System.out.println("-----------Fill in a String array with the elements from the ArrayList");
		String s2[] = new String[l1.size()];
		l1.toArray(s2);
		for (int l = 0; l < s.length; l++) {
			System.out.println(s[l]);
		}

		// Car
		Car c1 = new Car("Panda", 150000, "Seat");
		Car c2 = new Car("Serie 500", 250000, "BMW");
		Car c3 = new Car("Series 220", 350000, "Mercedes");

		c1.TurnLeft();

		ArrayList<Car> c = new ArrayList<Car>();
		c.add(c1);
		c.add(c2);
		c.add(c3);

		System.out.println("Si deixem un metode de cotxe buit no es queixa sempre i cuant el definim");

		// -------------------- METODES COTXE ------------------------
		Iterator<Car> iterC = c.iterator();

		// Displaying the values after iterating
		// through the list
		System.out.println("\nThe iterator values" + " of Car list are: ");
		// loop3
		for (int m = 0; m < c.size(); m++) {
			System.out.println(c.get(m).brand + " " + c.get(m).name);
		}
		
		// for normal
		for (Car x : c) {
			System.out.println(x.brand);
		}
	}
}
