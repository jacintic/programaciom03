package com.daw.jacintiglesias.krona;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import com.sun.source.tree.DoWhileLoopTree;

import java.util.List;
import java.util.ListIterator;
import java.util.Scanner;

// import cadena
import Llibreries.Cadena;

public class Waypoint {

	// methods
	public static ComprovacioRendiment inicialitzarComprovacioRendiment() {
		ComprovacioRendiment comprovacioRendimenTmp = new ComprovacioRendiment();
		return comprovacioRendimenTmp;
	}

	public static ComprovacioRendiment comprovarRendimentInicialitzacio(int numObjACrear,
			ComprovacioRendiment comprovacioRendimenTmp) {
		// contador temps en milisegons
		long tempsEnMs;
		// contador de temps
		long tempsInicialArr = System.nanoTime();
		// lista arrayList
		for (int i = 0; i < numObjACrear; i++) {
			int[] arr = { 0, 0, 0 };
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
			Waypoint_Dades wd1 = new Waypoint_Dades(0, "Òrbita de la Terra", arr, true,
					LocalDateTime.parse("15-08-1954 00:01", formatter), null, null);
			comprovacioRendimenTmp.addListArr(wd1);
		}
		// temps final linked list
		long tempsFinalArr = System.nanoTime();
		tempsEnMs = TimeUnit.MILLISECONDS.convert((tempsFinalArr - tempsInicialArr), TimeUnit.NANOSECONDS);
		System.out.println("Temps total de insertar " + numObjACrear + " elements en ArrayList: " + tempsEnMs);
		// contador de temps
		long tempsInicialLinked = System.nanoTime();
		// linked list
		for (int i = 0; i < numObjACrear; i++) {
			int[] arr = { 0, 0, 0 };
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
			Waypoint_Dades wd1 = new Waypoint_Dades(0, "Òrbita de la Terra", arr, true,
					LocalDateTime.parse("15-08-1954 00:01", formatter), null, null);
			comprovacioRendimenTmp.addListLinked(wd1);
		}
		long tempsFinalLinked = System.nanoTime();
		tempsEnMs = TimeUnit.MILLISECONDS.convert((tempsFinalLinked - tempsInicialLinked), TimeUnit.NANOSECONDS);
		System.out.println("Temps total de insertar " + numObjACrear + " elements en LinkedList: " + tempsEnMs);
		return comprovacioRendimenTmp;
	}

	public static ComprovacioRendiment comprovarRendimentInsercio(ComprovacioRendiment comprovacioRendimentTmp) {
		// object to insert
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		int[] arr = { 0, 0, 0 };
		Waypoint_Dades wd1 = new Waypoint_Dades(0, "Òrbita de la Terra", arr, true,
				LocalDateTime.parse("15-08-1954 00:01", formatter), null, null);
		int meitatllista = comprovacioRendimentTmp.llistaArrayList.size() / 2;
		// Print list size
		System.out.println("ArrayList.size(): " + comprovacioRendimentTmp.llistaArrayList.size() + " meitatllista: "
				+ meitatllista);
		long tempsInicialArr = System.nanoTime();
		comprovacioRendimentTmp.llistaArrayList.add(0, wd1);
		long tempsFinalArr = System.nanoTime();
		long tempsInicialLink = System.nanoTime();
		comprovacioRendimentTmp.llistaLinkedList.add(0, wd1);
		long tempsFinalLink = System.nanoTime();
		System.out.println("Temps insertar 1 element a la posició 0 ArrayList: " + (tempsFinalArr - tempsInicialArr));
		System.out
				.println("Temps insertar 1 element a la posició 0 LinkedList: " + (tempsFinalLink - tempsInicialLink));
		System.out.println("-----------------------------------------------------------------------");
		tempsInicialArr = System.nanoTime();
		comprovacioRendimentTmp.llistaArrayList.add(meitatllista, wd1);
		tempsFinalArr = System.nanoTime();
		tempsInicialLink = System.nanoTime();
		comprovacioRendimentTmp.llistaLinkedList.add(meitatllista, wd1);
		tempsFinalLink = System.nanoTime();
		System.out.println("Temps insertar 1 element a la posició " + meitatllista + " ArrayList: "
				+ (tempsFinalArr - tempsInicialArr));
		System.out.println("Temps insertar 1 element a la posició " + meitatllista + " LinkedList: "
				+ (tempsFinalLink - tempsInicialLink));
		System.out.println("-----------------------------------------------------------------------");
		tempsInicialArr = System.nanoTime();
		comprovacioRendimentTmp.llistaArrayList.add(wd1);
		tempsFinalArr = System.nanoTime();
		tempsInicialLink = System.nanoTime();
		comprovacioRendimentTmp.llistaLinkedList.add(wd1);
		tempsFinalLink = System.nanoTime();
		System.out.println("Temps insertar 1 element a la posició final: " + (tempsFinalArr - tempsInicialArr));
		System.out.println(
				"Temps insertar 1 element a la posició final:  LinkedList: " + (tempsFinalLink - tempsInicialLink));
		System.out.println("-----------------------------------------------------------------------");

		return comprovacioRendimentTmp;
	}

	public static ComprovacioRendiment modificarWaypoints(ComprovacioRendiment comprovacioRendimentTmp) {
		System.out.println("------------APARTAT1---------------");
		int sizeOfList = comprovacioRendimentTmp.llistaArrayList.size();
		for (int i = 0; i < sizeOfList; i++) {
			comprovacioRendimentTmp.idsPerArrayList.add(i);
		}
		System.out.println("El primer element te valor: " + comprovacioRendimentTmp.idsPerArrayList.get(0));
		System.out.println("El ultim element valor: " + comprovacioRendimentTmp.idsPerArrayList.get(sizeOfList - 1));

		System.out.println("------------APARTAT2---------------");
		for (Integer element : comprovacioRendimentTmp.idsPerArrayList) {
			System.out.println("Abans del canvi: comprovacioRendimentTmp.llistaArrayList.get(" + element + ").getId(): "
					+ comprovacioRendimentTmp.llistaArrayList.get(element).getId());
			comprovacioRendimentTmp.llistaArrayList.get(element).setId(element);
			System.out.println("Despres del canvi: comprovacioRendimentTmp.llistaArrayList.get(" + element
					+ ").getId(): " + comprovacioRendimentTmp.llistaArrayList.get(element).getId());
			System.out.println();
		}
		System.out.println("------------APARTAT3.1 (bucle for)---------------");
		for (int i = 0; i < comprovacioRendimentTmp.llistaArrayList.size(); i++) {
			System.out.println("ID = " + comprovacioRendimentTmp.llistaArrayList.get(i).getId() + ", nom = "
					+ comprovacioRendimentTmp.llistaArrayList.get(i).getNom());
		}
		System.out.println("------------APARTAT3.1 (Iterator)---------------");
		Iterator<Waypoint_Dades> iter = comprovacioRendimentTmp.llistaArrayList.iterator();
		while (iter.hasNext()) {
			Waypoint_Dades wp = iter.next();
			System.out.println("ID = " + wp.getId() + ", nom = " + wp.getNom());
		}
		System.out.println("------------APARTAT 4 (Iterator)---------------");
		System.out.println("Preparat per esborrar el contingut de llistaLinkedList que té "
				+ comprovacioRendimentTmp.llistaLinkedList.size() + " elements.");
		comprovacioRendimentTmp.llistaLinkedList.clear();
		System.out.println(
				"Esborrada. Ara llistaLinkedList té " + comprovacioRendimentTmp.llistaLinkedList.size() + " elements.");
		Iterator<Waypoint_Dades> iter2 = comprovacioRendimentTmp.llistaArrayList.iterator();
		while (iter2.hasNext()) {
			comprovacioRendimentTmp.llistaLinkedList.add(iter2.next());
		}
		System.out.println("Copiats els elements de llistaArrayList en llistaLinkedList que ara te "
				+ comprovacioRendimentTmp.llistaLinkedList.size() + " elements");
		System.out.println("------------APARTAT 5.1 (bucle for)---------------");
		for (Integer element : comprovacioRendimentTmp.idsPerArrayList) {
			if (comprovacioRendimentTmp.llistaArrayList.get(element).getId() > 5) {
				comprovacioRendimentTmp.llistaArrayList.get(element).setNom("Òrbita de Mart");
			}
			System.out.println("Modificat el waypoint amb id = " + element);
		}
		System.out.println();
		System.out.println("------------APARTAT 5.1 (comprovació)---------------");
		for (int i = 0; i < comprovacioRendimentTmp.llistaArrayList.size(); i++) {
			Waypoint_Dades wp = comprovacioRendimentTmp.llistaArrayList.get(i);
			System.out.println("El Waypoint amb id " + wp.getId() + " té el nom = " + wp.getNom());
		}
		System.out.println("------------APARTAT 5.2 (Iterator)---------------");
		Iterator<Waypoint_Dades> iter3 = comprovacioRendimentTmp.llistaLinkedList.iterator();
		while (iter3.hasNext()) {
			Waypoint_Dades wp = iter3.next();
			if (wp.getId() < 5) {
				wp.setNom("Punt Lagrange entre la Terra i la Lluna");
				System.out.println("Modificat el waypoint amb id = " + wp.getId());
			}
		}
		System.out.println();
		System.out.println("------------APARTAT 5.2 (comprovació)---------------");
		Iterator<Waypoint_Dades> iter4 = comprovacioRendimentTmp.llistaLinkedList.iterator();
		while (iter4.hasNext()) {
			Waypoint_Dades wp = iter4.next();
			System.out.println("El waypoint amb id = " + wp.getId() + " té el nom = " + wp.getNom());
		}
		return comprovacioRendimentTmp;
	}

	public static ComprovacioRendiment esborrarWaypoints(ComprovacioRendiment comprovacioRendimentTmp) {
		System.out.println(
				"S'esborraran waypoints de l'ArrayList i del LinkedList. Hi ha 3 apartats per a fer:APARTAT 1:Recorre la llista comprovacioRendimentTmp.llistaArrayList i suprimeix els waypoints amb IDmenors de 6 (amb for(Integer element: numbersList)). ¿Es pot? ¿Perquè?");
		System.out.println("No es pot per que la llista no es de Integer sino de Wapoint_Dades");
		System.out.println();
		System.out.println("---- APARTAT 2 (Iterator)");
		Iterator<Waypoint_Dades> iter5 = comprovacioRendimentTmp.llistaLinkedList.iterator();
		while (iter5.hasNext()) {
			Waypoint_Dades wp = iter5.next();
			if (wp.getId() > 4) {
				iter5.remove();
				System.out.println("Esborrat el waypoint amb id = " + wp.getId());
			}
		}
		System.out.println("---- APARTAT 2 (Comprovació)");
		Iterator<Waypoint_Dades> iter6 = comprovacioRendimentTmp.llistaLinkedList.iterator();
		while (iter6.hasNext()) {
			Waypoint_Dades wp = iter6.next();
			System.out.println("El waypoint amb id = " + wp.getId() + " té el nom = " + wp.getNom());
		}
		System.out.println("---- APARTAT 3 (listIterator)");
		ListIterator<Waypoint_Dades> iter7 = comprovacioRendimentTmp.llistaLinkedList.listIterator();
		while (iter7.hasNext()) {
			Waypoint_Dades wp = iter7.next();
			if (wp.getId() == 2) {
				System.out.println("Esborrat el waypoint amb id = " + wp.getId());
				iter7.remove();
			}
		}
		System.out.println("---- APARTAT 3 (comprovació)");
		while (iter7.hasPrevious()) {
			Waypoint_Dades wp = iter7.previous();
			System.out.println("El waypoint amb id = " + wp.getId() + " té el nom = " + wp.getNom());
		}
		return comprovacioRendimentTmp;
	}

	// lists pt 1 pt 2 ex4
	public static ComprovacioRendiment modificarCoordenadesINomDeWaypoints(
			ComprovacioRendiment comprovacioRendimentTmp) {
		// recorre llistaArraylist, modificar parells
		Iterator<Waypoint_Dades> iter8 = comprovacioRendimentTmp.llistaArrayList.iterator();
		while (iter8.hasNext()) {
			Waypoint_Dades wp = iter8.next();
			if (wp.getId() % 2 == 0) {
				System.out.println("----- Modificar el waypoint amb id = " + wp.getId() + " -----");
				System.out.println("Nom actual: " + wp.getNom());
				System.out.print("Nom nou: ");
				Scanner sc = new Scanner(System.in);
				String i = sc.nextLine();
				// cambiant el nom del atribut nom
				wp.setNom(i);
				System.out.println("Coordenades actuals: " + wp.getCoordenades()[0] + " " + wp.getCoordenades()[1] + " "
						+ wp.getCoordenades()[2]);
				System.out.print("Coordenades noves (format: 1 13 7): ");
				String coordsStr = sc.nextLine();
				String[] coodsArr = coordsStr.split(" ");
				while (coodsArr.length != 3 || !Cadena.stringIsInt(coodsArr[0])
						|| !Cadena.stringIsInt(coodsArr[1])
						|| !Cadena.stringIsInt(coodsArr[2])) {
					if (coodsArr.length != 3) {
						System.out.println("ERROR: Introduir 3 paràmetres separats per 1 espai en blanc. Has introduit "
								+ coodsArr.length + " paràmetres");
					}
					if (!Cadena.stringIsInt(coordsStr) && coodsArr.length > 1) {
						System.out.println("Coordenada " + Cadena.nonnonValidCoord(coodsArr[0]) + " "
								+ Cadena.nonnonValidCoord(coodsArr[1]) + " "
								+ Cadena.nonnonValidCoord(coodsArr[2]) + " no valida.");
					}
					System.out.println("Coordenades actuals: " + wp.getCoordenades()[0] + " " + wp.getCoordenades()[1]
							+ " " + wp.getCoordenades()[2]);
					System.out.print("Coordenades noves (format: 1 13 7): ");
					coordsStr = sc.nextLine();
					coodsArr = coordsStr.split(" ");
				}
				int[] intArr = new int[3];
				for (int counter = 0; counter < wp.getCoordenades().length; counter++) {
					intArr[counter] = Integer.parseInt(coodsArr[counter]);
				}
				wp.setCoordenades(intArr);
				System.out.println(Arrays.toString(wp.getCoordenades()));
				System.out.println();
			}
			
		}
		return comprovacioRendimentTmp;
	}
	// metode exercici 5
	public static ComprovacioRendiment visualitzarWaypointsOrdenats(ComprovacioRendiment comprovacioRendimentTmp) {
		Collections.sort(comprovacioRendimentTmp.llistaArrayList);
		Iterator<Waypoint_Dades> iterSort = comprovacioRendimentTmp.llistaArrayList.iterator();
		while (iterSort.hasNext()) {
			System.out.println(iterSort.next());
		}
		return comprovacioRendimentTmp;
	}
	// metode que realitza la accio de metode 6
	public static int calculaCoordenades(int[] coordenada) {
		int total = 0;
		for (int i = 0; i < coordenada.length; i++) {
			total += Math.pow(coordenada[i],2);
		}
		return total;
	}
	// metode 6
	public static ComprovacioRendiment waypointsACertaDistanciaMaxDeLaTerra(ComprovacioRendiment comprovacioRendimentTmp) {
		System.out.print("Distància màxima de la terra: ");
		Scanner sc = new Scanner(System.in);
		int distancia = sc.nextInt();
		Collections.sort(comprovacioRendimentTmp.llistaArrayList);
		Iterator<Waypoint_Dades> distIt = comprovacioRendimentTmp.llistaArrayList.iterator();
		while(distIt.hasNext()) {
			Waypoint_Dades wp = distIt.next();
			System.out.println(wp);
		}
		return comprovacioRendimentTmp;
	}

}
