package com.daw.jacintiglesias.krona;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.text.Collator;
import java.util.Locale;
import java.util.List;

public class Waypoint_Dades implements Comparable<Waypoint_Dades>{
	
	// attributes
	private int id;                     
	private String nom;
	private int[] coordenades;
	private boolean actiu;              
	private LocalDateTime dataCreacio;
	private LocalDateTime dataAnulacio;      
	private LocalDateTime dataModificacio;
	
	// constructor
	protected Waypoint_Dades(int id, String nom, int[] coordenades, boolean actiu, LocalDateTime dataCreacio,LocalDateTime dataAnulacio, LocalDateTime dataModificacio) {
		super();
		this.id = id;
		this.nom = nom;
		this.coordenades = coordenades;
		this.actiu = actiu;
		this.dataCreacio = dataCreacio;
		this.dataAnulacio = dataAnulacio;
		this.dataModificacio = dataModificacio;
	}
	protected Waypoint_Dades(LocalDateTime dataCreacio) {
		this.dataCreacio = dataCreacio;
	}

	@Override
	public String toString() {
		/*
		return "Waypoint_Dades [id=" + id + ", nom=" + nom + ", coordenades=" + Arrays.toString(coordenades)
				+ ", actiu=" + actiu + ", dataCreacio=" + dataCreacio + ", dataAnulacio=" + dataAnulacio
				+ ", dataModificacio=" + dataModificacio + "]";
		*/
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		String dc;
		String da;
		String dm;
		if (dataCreacio == null) {
			dc = "NULL";
		} else {
			dc = dataCreacio.format(formatter);
		}
		if (dataAnulacio == null) {
			da = "NULL";
		} else {
			da = dataAnulacio.format(formatter);
		}
		if (dataModificacio == null) {
			dm = "NULL";
		} else {
			dm = dataModificacio.format(formatter);
		}
		return "WAYPOINT " + id + ":\n" +
				"\t nom = " + nom + "\n" +
				"\t coordenades(x,y,z) = ("  +  coordenades[0] + "," + coordenades[1] + "," + coordenades[2] + ") (distància = " + (Math.pow(coordenades[0], 2) + Math.pow(coordenades[1], 2) + Math.pow(coordenades[2], 2) ) + ")\n" +
				"\tactiu = " + actiu + "\n" +  
				"\tdataCreacio = " + dc + "\n" +
				"\tdataAnulacio = " + da + "\n" +
				"\tdataModificacio = " + dm + "\n";
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public int[] getCoordenades() {
		return coordenades;
	}
	public void setCoordenades(int[] coordenades) {
		this.coordenades = coordenades;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	/// afegit hashcode i equals per a Krona 3.0, Deque, exercici 7
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (actiu ? 1231 : 1237);
		result = prime * result + Arrays.hashCode(coordenades);
		result = prime * result + ((dataAnulacio == null) ? 0 : dataAnulacio.hashCode());
		result = prime * result + ((dataCreacio == null) ? 0 : dataCreacio.hashCode());
		result = prime * result + ((dataModificacio == null) ? 0 : dataModificacio.hashCode());
		result = prime * result + id;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Waypoint_Dades other = (Waypoint_Dades) obj;
		if (actiu != other.actiu)
			return false;
		if (!Arrays.equals(coordenades, other.coordenades))
			return false;
		if (dataAnulacio == null) {
			if (other.dataAnulacio != null)
				return false;
		} else if (!dataAnulacio.equals(other.dataAnulacio))
			return false;
		if (dataCreacio == null) {
			if (other.dataCreacio != null)
				return false;
		} else if (!dataCreacio.equals(other.dataCreacio))
			return false;
		if (dataModificacio == null) {
			if (other.dataModificacio != null)
				return false;
		} else if (!dataModificacio.equals(other.dataModificacio))
			return false;
		if (id != other.id)
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		return true;
	}
	@Override
	public int compareTo(Waypoint_Dades wpd) {
		// SORT EXERCICI 5 (per coordenades i nom)
		///////////////////////////////////////////
		//////// descomentar per fer l'apartat de sort by coords and name //////////////
		/*
		int totalThisCoords = this.coordenades[0] + this.coordenades[1] + this.coordenades[2];
		int totalCompCoords = wpd.coordenades[0] + wpd.coordenades[1] + wpd.coordenades[2];
		if (totalThisCoords < totalCompCoords) {
			return -1;
		} else if (totalThisCoords == totalCompCoords) {
			//
			List compLiNom = List.of(this.nom, wpd.nom);
			List sortedTertiaryCollator = new ArrayList(compLiNom);
			Collator tertiaryCollator = Collator.getInstance(new Locale("es"));
			tertiaryCollator.setStrength(Collator.TERTIARY);
			sortedTertiaryCollator.sort(tertiaryCollator);
			if (sortedTertiaryCollator.get(0).equals(this.nom) ) {
				return -1;
			}
			return 1;
			
		}
		return 0;
		*/
		
		// SORT EXERCICI 5 (per coordenades i nom)
		///////////////////////////////////////////
		//////// comentar per fer l'apartat de sort by coords and name //////////////
		if (Waypoint.calculaCoordenades(this.getCoordenades()) > Waypoint.calculaCoordenades(wpd.getCoordenades())) {
			return -1;
		} else if (Waypoint.calculaCoordenades(this.getCoordenades()) < Waypoint.calculaCoordenades(wpd.getCoordenades())) {
			return 1;
		}
		return 0;
	}
    
    

    
    
}
