package com.daw.jacintiglesias.krona;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Stack;

import Varies.Data;

public class Ruta {
	private static int id;                             	
	private static String nom;
	private static ArrayList<Integer> waypoints;       	
	private static boolean actiu;                      	
	private static LocalDateTime dataCreacio;
	private static LocalDateTime dataAnulacio;             
	private static LocalDateTime dataModificacio;
	
	// constructor
	protected Ruta(int id, String nom, ArrayList<Integer> waypoints, boolean actiu, LocalDateTime dataCreacio,
			LocalDateTime dataAnulacio, LocalDateTime dataModificacio) {
		super();
		this.id = id;
		this.nom = nom;
		this.waypoints = waypoints;
		this.actiu = actiu;
		this.dataCreacio = dataCreacio;
		this.dataAnulacio = dataAnulacio;
		this.dataModificacio = dataModificacio;
	}
	///////// EXERCICI 10 (4) /////////
	// methods
	public static List<Waypoint_Dades> crearRutaInicial(ComprovacioRendiment cRtmp) {
		return cRtmp.llistaArrayList;
	}
	// executar el menu 1 previament
	public static ComprovacioRendiment inicialitarRuta(ComprovacioRendiment comprovacioRendimentTmp) {
		List rutaInicialTmp = crearRutaInicial(comprovacioRendimentTmp);
		Iterator<Waypoint_Dades> iter = rutaInicialTmp.iterator();
		while (iter.hasNext()) {
			Waypoint_Dades wpdEl = iter.next();
			comprovacioRendimentTmp.pilaWaypoints.push(wpdEl);
		}
		Waypoint_Dades wpDeque = new Waypoint_Dades(4, "Òrbita de Júpiter REPETIDA", new int[] {4,4,4}, true,Data.formatData("21-10-2020 00:30"), null,Data.formatData("22-10-2020 23:55"));
		comprovacioRendimentTmp.pilaWaypoints.addFirst(wpDeque);
		comprovacioRendimentTmp.wtmp = wpDeque;
		return comprovacioRendimentTmp;
	}
	///////// EXERCICI 11 (5) /////////
	public static void visualitzarRuta(ComprovacioRendiment comprovacioRendimentTmp){
		System.out.println(comprovacioRendimentTmp.pilaWaypoints);
	}
	
	///////// EXERCICI 12 (6) /////////
	public static void invertirRuta(ComprovacioRendiment comprovacioRendimentTmp) {
		Deque<Waypoint_Dades> pw = comprovacioRendimentTmp.pilaWaypoints;
		Deque<Waypoint_Dades> pilaWaypointsInversa = new ArrayDeque<Waypoint_Dades>();
		while (!pw.isEmpty()) {
			pilaWaypointsInversa.push(pw.pop());
		}
		System.out.println(pilaWaypointsInversa);
	}
	
	///////// EXERCICI 13 (7) /////////
	public static void existeixWaypointEnRuta(ComprovacioRendiment comprovacioRendimentTmp) {
		
		System.out.println("Is wtmp found in pilaWaypoints? " + comprovacioRendimentTmp.pilaWaypoints.contains(comprovacioRendimentTmp.wtmp));
		// creating new waypoint identic to wtmp
		Waypoint_Dades wpDeque = new Waypoint_Dades(4, "Òrbita de Júpiter REPETIDA", new int[] {4,4,4}, true,Data.formatData("21-10-2020 00:30"), null,Data.formatData("22-10-2020 23:55"));
		System.out.println("Is new Waypoint identic to wtmp found in pilaWaypoints? " + comprovacioRendimentTmp.pilaWaypoints.contains(wpDeque));
	}
	
	// getters
	public static int getId() {
		return id;
	}

	public static String getNom() {
		return nom;
	}

	public static ArrayList<Integer> getWaypoints() {
		return waypoints;
	}

	public static boolean isActiu() {
		return actiu;
	}

	public static LocalDateTime getDataCreacio() {
		return dataCreacio;
	}

	public static LocalDateTime getDataAnulacio() {
		return dataAnulacio;
	}

	public static LocalDateTime getDataModificacio() {
		return dataModificacio;
	}
}
