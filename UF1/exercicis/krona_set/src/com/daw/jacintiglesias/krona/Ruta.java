package com.daw.jacintiglesias.krona;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Scanner;
import java.util.Set;
import java.util.Stack;
import java.util.TreeSet;

import Varies.Data;

public class Ruta{
	private static int id;
	private static String nom;
	private static ArrayList<Integer> waypoints;
	private static boolean actiu;
	private static LocalDateTime dataCreacio;
	private static LocalDateTime dataAnulacio;
	private static LocalDateTime dataModificacio;

	// constructor
	protected Ruta(int id, String nom, ArrayList<Integer> waypoints, boolean actiu, LocalDateTime dataCreacio,
			LocalDateTime dataAnulacio, LocalDateTime dataModificacio) {
		super();
		this.id = id;
		this.nom = nom;
		this.waypoints = waypoints;
		this.actiu = actiu;
		this.dataCreacio = dataCreacio;
		this.dataAnulacio = dataAnulacio;
		this.dataModificacio = dataModificacio;
	}

	///////// EXERCICI 10 (4) /////////
	// methods
	public static List<Waypoint_Dades> crearRutaInicial(ComprovacioRendiment cRtmp) {
		return cRtmp.llistaArrayList;
	}

	// executar el menu 1 previament
	public static ComprovacioRendiment inicialitarRuta(ComprovacioRendiment comprovacioRendimentTmp) {
		List rutaInicialTmp = crearRutaInicial(comprovacioRendimentTmp);
		Iterator<Waypoint_Dades> iter = rutaInicialTmp.iterator();
		while (iter.hasNext()) {
			Waypoint_Dades wpdEl = iter.next();
			comprovacioRendimentTmp.pilaWaypoints.push(wpdEl);
		}
		Waypoint_Dades wpDeque = new Waypoint_Dades(4, "Òrbita de Júpiter REPETIDA", new int[] { 4, 4, 4 }, true,
				Data.formatData("21-10-2020 00:30"), null, Data.formatData("22-10-2020 23:55"));
		comprovacioRendimentTmp.pilaWaypoints.addFirst(wpDeque);
		comprovacioRendimentTmp.wtmp = wpDeque;
		return comprovacioRendimentTmp;
	}

	///////// EXERCICI 11 (5) /////////
	public static void visualitzarRuta(ComprovacioRendiment comprovacioRendimentTmp) {
		System.out.println(comprovacioRendimentTmp.pilaWaypoints);
	}

	///////// EXERCICI 12 (6) /////////
	public static void invertirRuta(ComprovacioRendiment comprovacioRendimentTmp) {
		Deque<Waypoint_Dades> pw = comprovacioRendimentTmp.pilaWaypoints;
		Deque<Waypoint_Dades> pilaWaypointsInversa = new ArrayDeque<Waypoint_Dades>();
		while (!pw.isEmpty()) {
			pilaWaypointsInversa.push(pw.pop());
		}
		System.out.println(pilaWaypointsInversa);
	}

	///////// EXERCICI 13 (7) /////////
	public static void existeixWaypointEnRuta(ComprovacioRendiment comprovacioRendimentTmp) {

		System.out.println("Is wtmp found in pilaWaypoints? "
				+ comprovacioRendimentTmp.pilaWaypoints.contains(comprovacioRendimentTmp.wtmp));
		// creating new waypoint identic to wtmp
		Waypoint_Dades wpDeque = new Waypoint_Dades(4, "Òrbita de Júpiter REPETIDA", new int[] { 4, 4, 4 }, true,
				Data.formatData("21-10-2020 00:30"), null, Data.formatData("22-10-2020 23:55"));
		System.out.println("Is new Waypoint identic to wtmp found in pilaWaypoints? "
				+ comprovacioRendimentTmp.pilaWaypoints.contains(wpDeque));
	}

	// krona set 4 20
	public static ComprovacioRendiment inicialitzaLListaRutes(ComprovacioRendiment comprovacioRendimentTmp) {

		Ruta_Dades ruta_0 = new Ruta_Dades(0, "ruta   0:   Terra   -->   Punt   Lagrange   Júpiter-Europa",
				new ArrayList<Integer>(Arrays.asList(0, 1, 2, 3, 4, 5)), true, Data.formatData("28-10-2020 16:30"),
				null, Data.formatData("28-10-2020 16:30"));
		Ruta_Dades ruta_1 = new Ruta_Dades(1, "ruta 1: Terra --> Òrbita de Mart (directe)",
				new ArrayList<Integer>(Arrays.asList(0, 3)), true, Data.formatData("28-10-2020 16:31"), null,
				Data.formatData("28-10-2020 16:31"));
		Ruta_Dades ruta_2 = new Ruta_Dades(2, "ruta 2.1: Terra --> Òrbita de Venus",
				new ArrayList<Integer>(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7)), true,
				Data.formatData("28-10-2020 16:32"), null, Data.formatData("28-10-2020 16:32"));
		Ruta_Dades ruta_3 = new Ruta_Dades(3, "ruta 3: Terra --> Mart (directe) --> Òrbita de Júpiter ",
				new ArrayList<Integer>(Arrays.asList(0, 3, 4)), true, Data.formatData("28-10-2020 16:33"), null,
				Data.formatData("28-10-2020 16:33"));
		Ruta_Dades ruta_4 = new Ruta_Dades(4, "ruta   2.2:   Terra   -->   Òrbita   de   Venus   (REPETIDA)",
				new ArrayList<Integer>(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7)), true,
				Data.formatData("28-10-2020 16:32"), null, Data.formatData("30-10-2020 19:49"));
		// add routes to arraylist
		comprovacioRendimentTmp.llistaRutes.add(ruta_0);
		comprovacioRendimentTmp.llistaRutes.add(ruta_1);
		comprovacioRendimentTmp.llistaRutes.add(ruta_2);
		comprovacioRendimentTmp.llistaRutes.add(ruta_3);
		comprovacioRendimentTmp.llistaRutes.add(ruta_4);
		Iterator<Ruta_Dades> rdi = comprovacioRendimentTmp.llistaRutes.iterator();
		while (rdi.hasNext()) {
			Ruta_Dades rd = rdi.next();
			System.out.println(rd.getNom() + " waypoints[" + rd.getWaypoints() + "]");
		}
		// comprovacioRendimentTmp.llistaRutes.forEach((route) ->
		// System.out.println(route));
		return comprovacioRendimentTmp;
	}

	// Krona set 5 21 Unio
	public static void setUnio(ComprovacioRendiment comprovacioRendimentTmp) {
		Set<Integer> waypoints = new <Integer>HashSet();

		comprovacioRendimentTmp.llistaRutes.forEach((ruta) -> waypoints.addAll(ruta.getWaypoints()));
		System.out.println("ID dels waypoints ficats en el set " + waypoints);
	}

	// Krona set 6 22 interseccio
	public static void setInterseccio(ComprovacioRendiment comprovacioRendimentTmp) {
		Set<Integer> waypointsIntersect = new <Integer>HashSet();
		Iterator<Ruta_Dades> ruDad = comprovacioRendimentTmp.llistaRutes.iterator();
		int counter = 0;
		for (int i = 0; i < comprovacioRendimentTmp.llistaRutes.size(); i++) {
			Ruta_Dades rdiCurr = comprovacioRendimentTmp.llistaRutes.get(i);
			if (i == 0) {
				waypointsIntersect.addAll(rdiCurr.getWaypoints());
			} else {
				Ruta_Dades rdiPrev = comprovacioRendimentTmp.llistaRutes.get(i - 1);
				waypointsIntersect.retainAll(rdiPrev.getWaypoints());
			}

		}
		System.out.println("ID dels waypoints en totes les rutes: " + waypointsIntersect);
	}

	// set 7 222 buscarruta
	public static int buscarRuta(int numRuta, ComprovacioRendiment comprovacioRendimentTmp) {
		List<Ruta_Dades> rd = comprovacioRendimentTmp.llistaRutes;
		Iterator<Ruta_Dades> rdIt = rd.iterator();
		while (rdIt.hasNext()) {
			Integer rdId = rdIt.next().getId();
			if (rdId == numRuta) {
				return rdId;
			}
		}
		return -1;
	}

	// set 8 23
	public static void setResta(ComprovacioRendiment comprovacioRendimentTmp) {
		System.out.println("Rutes actuals:");
		comprovacioRendimentTmp.llistaRutes.forEach((ruta) -> System.out
				.println("ID: " + ruta.getId() + " ruta: " + ruta.getNom() + ": waypoints " + ruta.getWaypoints()));

		Scanner sc = new Scanner(System.in);
		boolean invalidRoute = false;
		String[] routesArr;
		do {
			invalidRoute = false;
			System.out.println("Selecciona ruta A i B (format: 3 17): ");
			String routes = sc.nextLine();
			routesArr = routes.split(" ");

			if (routesArr.length > 2) {
				System.out.println("ERROR: Introduir 2 parametres separats per un espai en blanc. Has introduit "
						+ routesArr.length + " arametres.");
				invalidRoute = true;
			} else {
				try {
					int A = Integer.parseInt(routesArr[0]);
				} catch (final NumberFormatException e) {
					System.out.println(
							"ERROR: Has introduit " + routesArr[0] + " com a ruta. Els ID de les rutes són integers.");
					invalidRoute = true;
				}
				try {
					int B = Integer.parseInt(routesArr[1]);
				} catch (final NumberFormatException e) {
					System.out.println(
							"ERROR: Has introduit " + routesArr[1] + " com a ruta. Els ID de les rutes són integers.");
					invalidRoute = true;
				}
				if (!invalidRoute) {
					if (buscarRuta(Integer.parseInt(routesArr[0]), comprovacioRendimentTmp) == -1) {
						System.out.println("ERROR: no existeix la ruta " + routesArr[0] + " en el sistema.");
						invalidRoute = true;
					}
					if (buscarRuta(Integer.parseInt(routesArr[1]), comprovacioRendimentTmp) == -1) {
						System.out.println("ERROR: no existeix la ruta " + routesArr[1] + " en el sistema.");
						invalidRoute = true;
					}
				}
			}

		} while (invalidRoute);
		Set<Integer> hsRouteA = null;
		Set<Integer> hsRouteB = null;
		List<Ruta_Dades> lRD = comprovacioRendimentTmp.llistaRutes;
		for (Ruta_Dades ruta : lRD) {
			if (ruta.getId() == Integer.parseInt(routesArr[0])) {
				hsRouteA = new HashSet<Integer>(ruta.getWaypoints());
			}
			if (ruta.getId() == Integer.parseInt(routesArr[1])) {
				hsRouteB = new HashSet<Integer>(ruta.getWaypoints());
			}
		}
		System.out.println("HashSet (havent-hi afegir els wayponts de la ruta A) = " + hsRouteA);
		System.out.println("HashSet (havent-hi afegir els wayponts de la ruta B) = " + hsRouteB);

	}
	/*
	class MyComparator implements Comparator<Ruta_Dades> {

		@Override
		public int compare(Ruta_Dades A, Ruta_Dades B) {
			
			/*
			 * Si 2 rutes tenen els mateixos waypoints, a llavors pel sistema 
			 * seran la mateixa ruta.
			 * 
			 * Si 2 rutes no tenen els mateixos waypoints, 
			 * a llavors s'ordenaran per l'ID (de ID més gran amés petit).*/
	/*
			if (A.getWaypoints().equals(B.getWaypoints())) {
				System.out.println("A i B son iguals " + A + "----" + B);
				return 0;
			} else if (A.getId() < B.getId()){
				return -1;
			} else if (A.getId() > B.getId()){
				return 1;
			}
			return 0;
		}

	}
	*/

	// set 9 24
	public static void crearSetOrdenatDeRutes(ComprovacioRendiment comprovacioRendimentTmp) {
		TreeSet<Ruta_Dades> tree = new TreeSet<Ruta_Dades>();
		tree.addAll(comprovacioRendimentTmp.llistaRutes);
		//comprovacioRendimentTmp.llistaRutes.forEach((ruta) -> tree.add(ruta));
		System.out.println(tree);

	}

	
	// getters
	public static int getId() {
		return id;
	}

	public static String getNom() {
		return nom;
	}

	public static ArrayList<Integer> getWaypoints() {
		return waypoints;
	}

	public static boolean isActiu() {
		return actiu;
	}

	public static LocalDateTime getDataCreacio() {
		return dataCreacio;
	}

	public static LocalDateTime getDataAnulacio() {
		return dataAnulacio;
	}

	public static LocalDateTime getDataModificacio() {
		return dataModificacio;
	}
/*
	@Override
	public int compare(Ruta_Dades A, Ruta_Dades B) {
		if (A.getWaypoints().equals(B.getWaypoints())) {
			System.out.println("A i B son iguals " + A + "----" + B);
			return 0;
		} else if (A.getId() < B.getId()){
			return -1;
		} else if (A.getId() > B.getId()){
			return 1;
		}
		return 0;
	}
	*/
}
