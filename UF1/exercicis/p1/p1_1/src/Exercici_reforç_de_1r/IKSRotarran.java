package Exercici_reforç_de_1r;

import java.time.LocalDateTime;

public class IKSRotarran {

	public static void main(String[] args) {
		///////////////////////
		////// EXERCICI 2///////
		///////////////////////
		////// CAPITA///////
		System.out.println("--------Exercici 2--------");
		Oficial o1 = new Oficial("0O1-A", "Martok", true, LocalDateTime.of(1954, 8, 15, 0, 1), 1, 1, true,
				"Capitanejar la nau");
		System.out.println(o1.ImprimirDadesTripulant());
		////// MARINER///////
		Mariner m1 = new Mariner("758-J", "Kurak", true, LocalDateTime.of(1981, 12, 26, 13, 42), 3, 1, true,
				"Mariner encarregat del timó i l anavegació duran el 2n torn");
		System.out.println(m1.ImprimirDadesTripulant());
		////// Accedir a departament capita///////
		System.out.println("IKSRotarranConstants Capità: " + o1.departament
				+ "Es pot accedir per que es protected y oficial es fill de tripulant");
		////// Accedir a descripcio feina capita directament///////
		System.out.println("Descripcio feina capita: "
				+ /* o1.descripcioFeina + */ "No es pot accedir per que es private, necesitariem un getter");
		////// Accedir a descripcio feina capita a traves de getter///////
		System.out.println(
				"Descripcio feina capita: " + o1.getDescripcioFeina() + " - He creat un getter per accedir-hi");
		////// IMPRIMIR DADES CAPITA///////
		System.out.println(o1.ImprimirDadesTripulant());
		////// CAMBIAR LLOC DEPARTAMENT CAPITA DIRECTAMENT///////
		o1.departament = 10;
		System.out.println("IKSRotarranConstants capittà (cambiat): " + o1.departament);
		// String ID, String nom, boolean actiu, LocalDateTime dataAlta, int
		// departament, int llocDeServei,boolean serveiEnPont,String descripcioFeina

		// format of LocalDateTime.of.(year,month,dayofMonth,hour,minute)
		System.out.println();
		///////////////////////
		////// EXERCICI 3///////
		///////////////////////
		System.out.println("--------Exercici 3--------");
		Tripulant oficialDeTipusTripulant = new Oficial("0O2-A", "Gnaarg", true, LocalDateTime.of(1954, 8, 15, 0, 1), 1,
				1, true, "Oficial Tripulant");
		Oficial oficialdeTipusOficial = new Oficial("003-A", "Aargh", true, LocalDateTime.of(1954, 8, 15, 0, 1), 1, 1,
				true, "Oficial tipus Oficial");
		System.out.println(oficialDeTipusTripulant.saludar());
		System.out.println(oficialdeTipusOficial.saludar());
		System.out.println();
		///////////////////////
		//////EXERCICI 4///////
		///////////////////////
		System.out.println("--------Exercici 4--------");
		Oficial oficialdeTipusOficial2 = new Oficial("003-A", "Glooq", true, LocalDateTime.of(1954, 8, 15, 0, 1), 1, 1,
				true, "Oficial tipus Oficial");
		System.out.println(oficialDeTipusTripulant.equals(oficialdeTipusOficial));
		System.out.println(oficialdeTipusOficial.equals(oficialdeTipusOficial2));
		System.out.println();
		///////////////////////
		//////EXERCICI 5///////
		///////////////////////
		System.out.println("--------Exercici 5--------");
		System.out.println(IKSRotarranConstants.LLOCS_DE_SERVEI[o1.getLlocDeServei()]);
		System.out.println();
		///////////////////////
		//////EXERCICI 6///////
		///////////////////////
		System.out.println("--------Exercici 6--------");
		System.out.println("Oficial amb toString():" + o1);
		System.out.println("Mariner sense toString():" + m1);
		System.out.println("El toString() implementat descriveix els atributs, sense toString() implementat mostra el contenidor i la direcció de memoria.");
		System.out.println();
		///////////////////////
		//////EXERCICI 7///////
		///////////////////////
		System.out.println("--------Exercici 7--------");
		System.out.println(m1.ImprimirDadesTripulant());
		
	}

}
