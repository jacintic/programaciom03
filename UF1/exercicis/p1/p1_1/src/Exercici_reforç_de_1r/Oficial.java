/**
 * 
 */
package Exercici_reforç_de_1r;

import java.time.LocalDateTime;

/**
 * @author yo
 *
 */
public class Oficial extends Tripulant {

	/**
	 * @param ID
	 * @param nom
	 * @param actiu
	 * @param dataAlta
	 * @param departament
	 * @param llocDeServei
	 */
	// attributes
		private boolean serveiEnPont;
		private String descripcioFeina;
		
	public Oficial(String ID, String nom, boolean actiu, LocalDateTime dataAlta, int departament, int llocDeServei,boolean serveiEnPont,String descripcioFeina) {
		super(ID, nom, actiu, dataAlta, departament, llocDeServei);
		// TODO Auto-generated constructor stub
		// main attributes
				this.serveiEnPont = serveiEnPont;
				this.setDescripcioFeina(descripcioFeina);
	}

	@Override
	protected String ImprimirDadesTripulant() {
		// TODO Auto-generated method stub
		
		return "Oficial [serveiEnPont=" + serveiEnPont + ", descripcioFeina=" + getDescripcioFeina() + ", ID=" + ID
				+ ", nom=" + nom + ", actiu=" + actiu + ", dataAlta=" +   myDateFormatter(dataAlta)     + ", departament=" + departament + "]";
	}

	private void serveixEnElPont(boolean serveiEnPont) {
		this.serveiEnPont = serveiEnPont;
	}

	public String getDescripcioFeina() {
		return descripcioFeina;
	}

	public void setDescripcioFeina(String descripcioFeina) {
		this.descripcioFeina = descripcioFeina;
	}
	// exercici 3
	@Override
	public String saludar() {
		return "Hola desde la classe Oficial";
	}

	@Override
	public String toString() {
		return "Oficial [serveiEnPont=" + serveiEnPont + ", descripcioFeina=" + descripcioFeina + ", ID=" + ID
				+ ", nom=" + nom + ", actiu=" + actiu + ", dataAlta=" + dataAlta + ", departament=" + departament + "]";
	}
}
