package Exercici_reforç_de_1r;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public abstract class Tripulant {
	//attributes
	protected static final String BANDOL = "Imperi Klingoln";
	protected String ID;
	protected String nom;
	protected boolean actiu;
	protected LocalDateTime dataAlta;
	protected int departament;
	private int llocDeServei;
	
	// constructor
	protected Tripulant(String ID, String nom, boolean actiu, LocalDateTime dataAlta, int departament, int llocDeServei) {
		this.ID = ID;
		this.nom = nom;
		this.actiu = actiu;
		this.dataAlta = dataAlta;
		this.departament = departament;
		this.llocDeServei = llocDeServei;
	}
	
	// methods
	protected abstract String ImprimirDadesTripulant();

	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getDepartament() {
		return departament;
	}

	public void setDepartament(int departament) {
		this.departament = departament;
	}
	public String myDateFormatter(LocalDateTime myDate) {
		DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		return FORMATTER.format(myDate);
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ID == null) ? 0 : ID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tripulant other = (Tripulant) obj;
		if (ID == null) {
			if (other.ID != null)
				return false;
		} else if (!ID.equals(other.ID))
			return false;
		return true;
	}

	// EXERCICI 3
	public String saludar() {
		return "Hola desde la classe Tripulant";
	}

	public int getLlocDeServei() {
		return llocDeServei;
	}
}
