package jacintexamenstreams;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;

public class ex2 {
	/*
	 * Opció 1) Crear un programa que donada una url (tria tu la URL o fes servir alguna ja vista a
		classe), agafi tots els links que contingui i els guardi en un fitxer de text. En un altre fitxer
		guardarem totes les dades de la pàgina eliminant tots els tags que trobem.
		Fes servir els Readers / Writers per a llegir i escriure en fitxer
		
		// links
		 * dades dins de tags
	 * */

	public static void main(String[] args) throws IOException {
		
		/// HE DECIDIT ITERAR 2 cops per tota la pagina i la primera iteració agafar nomes links i la segona agafar nomes tags
		String url = "http://www.escoladeltreball.org";
		BufferedReader br = null;
		String ROOT_DIR = "ex2Dir/";
		// creating root dir
		File dir = new File(ROOT_DIR);
		if (dir.exists()) {
			System.out.println("El directori de ex2Dir existeix, skipping.");
		} else {
			try {
				FileUtils.forceMkdir(dir);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		
		try {
			URL miUrl = new URL(url);

			InputStreamReader in = new InputStreamReader(miUrl.openStream()); 
			br = new BufferedReader(in);

			String inputLine = "";

			while ( (inputLine = br.readLine() )!= null ){
				if (inputLine.matches(".*href.*")){
					//System.out.println(inputLine);
					// String to be scanned to find the pattern.
				      String pattern = "(a href=\")([^\"]+)";
				      // patern for tags
				      String pattern2 = "(>)([^<]+)";

				      // Create a Pattern object
				      Pattern r = Pattern.compile(pattern);

				      // Now create matcher object.
				      Matcher m = r.matcher(inputLine);
				      
				      if (m.find( )) {
				         //System.out.println("Link: " + m.group(2) );
				      // print file
				 		boolean append = true;
				 		FileWriter fw = new FileWriter(ROOT_DIR + "linkCollection.txt", append);
				 		PrintWriter pw = new PrintWriter(fw, true);
				 		try {
				 			// print link line to file
				 			pw.println(m.group(2));
				 		
				 		} catch (Exception e) {
				 			System.out.println(e.getMessage());
				 		} finally {
				 			fw.close();
				 		}
				      } 
				}
		
			}


		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			br.close();
		}
		try {
			URL miUrl = new URL(url);

			InputStreamReader in = new InputStreamReader(miUrl.openStream()); 
			br = new BufferedReader(in);

			String inputLine = "";

			while ( (inputLine = br.readLine() )!= null ){
				if (inputLine.matches(".*href.*")){
					//System.out.println(inputLine);
					// String to be scanned to find the pattern.
				      String pattern = "(>)([^<]+)(</)";
				      // Create a Pattern object
				      Pattern r = Pattern.compile(pattern);

				      // Now create matcher object.
				      Matcher m = r.matcher(inputLine);
				      
				      if (m.find( )) {
				         //System.out.println("Link: " + m.group(2) );
				      // print file
				 		boolean append = true;
				 		FileWriter fw = new FileWriter(ROOT_DIR + "linkCollection.txt", append);
				 		PrintWriter pw = new PrintWriter(fw, true);
				 		try {
				 			// print link line to file
				 			pw.println(m.group(2));
				 		
				 		} catch (Exception e) {
				 			System.out.println(e.getMessage());
				 		} finally {
				 			fw.close();
				 		}
				      } 
				}
		
			}


		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			br.close();
		}
	}
}
