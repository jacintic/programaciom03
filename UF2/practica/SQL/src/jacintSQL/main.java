package jacintSQL;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import business.Client;
import sql.conf.ClientJDBCDAO;
import sql.conf.ComandaJDBCDAO;
import sql.conf.DAOException;

public class main {

	public static void main(String[] args) throws DAOException, SQLException, IOException {

		// 1. JDBC

		System.out.println("1.0 -- get all clients");
		ClientJDBCDAO test = new ClientJDBCDAO();
		boolean conection = test.connect();
		System.out.println(conection ? "Connection stablished" : "Unable to connect");
		if (test.isConnected()) {
			// .0 get all clients
			System.out.println("Print clients table");
			test.printTablaClientes();

			System.out.println("1.0 -- get client by id (9)");
			// .1 get client by id
			System.out.println(test.getClientById(9));
			
			System.out.println("1.0 -- update Client with id 3");
			System.out.println(test.updateCliente(3, "Manuel", "Penjamon") ?"Client updtaed successfully": "No columns affected");
			System.out.println("Verifying update.");
			System.out.println(test.getClientById(3));
			
			
			System.out.println("Inserting client"); test.insertCliente("Manolin","Albacete");
			System.out.println("Verifying insert");
			test.printTablaClientes();
			
			System.out.println("Deleting client"); 
			test.deleteCliente(8);
			System.out.println(test.getClientById(8) == null? "Client deleted sucessfully" : "Client not deleted");
			
			
			
			
			
			
			test.close();
			
			System.out.println(test.isConnected()? "Problem closing connection" : "Connection closed sucessfully");
			
			// COMANDES AMB JOIN

			System.out.println("ORDERS WITH JOIN");
			
			ComandaJDBCDAO com = new ComandaJDBCDAO();
			boolean conection2 = com.connect();
			System.out.println(conection2 ? "Connection stablished" : "Unable to connect");
			if (com.isConnected()) {
			
				System.out.println("PRINTING ORDERS");
				
				com.prtingComandes(com.getComandes());
				com.close();
				System.out.println(com.isConnected()? "Problem closing connection" : "Connection closed sucessfully");
			}
		}


	}

}
