package exceptions0;

public class Exercici3 {
	/*
	 * EXERCICI 3:
		Fes un mètode anomenat divisió. Aquest mètode crearà un objecte Excepció 
		en cas de que els divisor sigui igual a 0. Crida el mètode divisió des del 
		main i encapsula el codi en un bloc try-catch. */
	public void divisio(int n) {
		try {
			if (n == 0) {
				throw new ArithmeticException();
			} else {
				System.out.println(2/n);
			}
		}catch (ArithmeticException e){
			System.out.println("ERROR: no es pot dividir per 0!");
		}
	}
}
