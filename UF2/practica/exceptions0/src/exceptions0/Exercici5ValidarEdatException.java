package exceptions0;

public class Exercici5ValidarEdatException extends Exception {
	@Override
	public String toString() {
		return "ERROR: l'edat es major a 100 o inferior a 0";
	}
}
