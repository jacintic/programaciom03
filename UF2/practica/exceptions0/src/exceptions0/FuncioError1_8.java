package exceptions0;

public class FuncioError1_8 {
	public void funcio3(int n) {
		System.out.println("funcio 3");
		String a = "" + "abc".charAt(n);
		System.out.println("Fi crida f3");
	}
	public void funcio2(int n) {
		System.out.println("funcio 2");
		try {
			if (n > 2 || n < 0 ) {throw new MyStringIndexOutOfBoundsException();}
			else {
				funcio3(n);
			}
		} catch (Exception e){
			   System.out.println(e);
		}
		System.out.println("Fi crida f2");
	}
	public void funcio1(int n) {
		System.out.println("funcio 1");
		funcio2(n);
		System.out.println("Fi crida f1");
	}
}
