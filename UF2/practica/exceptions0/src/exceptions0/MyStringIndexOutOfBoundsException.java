package exceptions0;

public class MyStringIndexOutOfBoundsException extends Exception{
	@Override
	public String toString() {
		return "Position of index inside string 'abc' is out of bounds!";
	}

}
