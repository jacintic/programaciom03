package jacint.daw;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Ex5 {

	/*
	 * EXERCICI 5 Donada una llista d'arxius, compta el número de línies i el número
	 * de caràcters de cada arxiu. El programa ha de mostrar per cada arxiu el nom,
	 * el número de línies i el número de caràcters. Si es produeix un error quan
	 * s'intenti llegir un d'aquests arxius s'ha d'imprimir un missatge d'error per
	 * aquest arxiu, però es continuarà processant la resta. Fes servir els Readers
	 * / Writers (apartat 1.3. dels apunts) per a llegir i escriure en fitxer. Fica
	 * la lista d'arxius a processar en un ArrayList i accedeix als seus noms
	 * recorrent la llista.
	 */

	public static void main(String[] args) throws IOException {
		// setting up root folder
		String ROOT_DIR = "ex5Dir";
		// loading list of files
		File f = new File(ROOT_DIR);
		File[] listOfFiles = f.listFiles();
		int fileCount = 0;
		// list of filenames
		List <String> filename = new ArrayList<String>();
		
		for (File myFile : listOfFiles) {
			File fitxer = new File(ROOT_DIR + "/" + myFile.getName()); // Adreçament relatiu
			FileReader fr = null; // Entrada
			FileReader fr2 = null;
			BufferedReader br; // Buffer
			BufferedReader br2; // Buffer
			String linea; // strings de linea de text
			char caracter;
			// obrir i llegir arxius
			try {
				fr = new FileReader(fitxer); // Inicialitza entrada del fitxer
				fr2 = new FileReader(fitxer);
				br = new BufferedReader(fr);
				br2 = new BufferedReader(fr2);// Inicialitza buffer amb l’entrada
				int contLineas = 0;
				int contCharacters = 0;
				// METODE DE LLEGIR MIXTE (FUNCIONA) //// <=====
				/*
				while ((linea = br.readLine()) != null ) {
					contLineas++;
					contCharacters += linea.length();
				}
				*/
				//// LLEGIR NOMES CARACTER A CARACTER
				int c;
				while ((c = br.read()) != -1 ) {
					contCharacters++;
				}
				/// METODE LLEGIR NOMES LINEAS
				while ((linea = br2.readLine()) != null ) {
					contLineas++;
				}
				
				filename.add(myFile.getName());
				System.out.println("Fitxer: " + filename.get(fileCount) + 
						"\nLineas: " + contLineas + 
						"\nCaracters: " + contCharacters + "\n");
				fileCount++;
			} catch (FileNotFoundException e) {
				System.out.println("Fitxer no existeix o no es pot llegir. HINT: no read permission.\n");
			} catch (IOException e) {
				System.out.println(e.getMessage());
			} finally {
				if (fr != null)
					fr.close();
				if (fr2 != null)
					fr2.close();
			}
		}
	}
}
