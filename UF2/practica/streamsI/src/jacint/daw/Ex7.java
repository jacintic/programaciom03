package jacint.daw;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class Ex7 {

	/*
	 * EXERCICI 7
		Crear un programa que llegeixi un fitxer de text, per a cada 
		caràcter guardarem les seves repeticions i guardarem un fitxer 
		de sortida que contingui. Caracter, repeticions, percentatge. 
		Fes servir els Readers / Writers (apartat 1.3. dels apunts) per 
		a llegir i escriure en fitxer i un HashMap on guardar els 
		caràcters llegits i les seves repeticions.
	 */

	public static void main(String[] args) throws IOException {
		String ROOT_DIR = "ex7Dir/";
		// store characters and occurrences here
		Map<String,Integer> chars = new HashMap<String,Integer>();
		// read lines and build hasmap character register
		File fitxer = new File(ROOT_DIR + "1.txt");     // Adreçament relatiu       
		FileReader fr = null;                     // Entrada    
		BufferedReader br;                        // Buffer       
		String linea;
		int[] total = {0};
		try {        
			fr = new FileReader(fitxer);           // Inicialitza entrada del fitxer        
			br = new BufferedReader(fr);           // Inicialitza buffer amb l’entrada        
			while((linea = br.readLine())!= null) {            
				String[] myCharStr = linea.split("");
				for (String myChar : myCharStr) {
					if (!myChar.equals(" ") && !myChar.equals("")) {
						if (!chars.containsKey(myChar)) {
							chars.put(myChar.toLowerCase(), 1);
						} else {
							chars.put(myChar.toLowerCase(),
									chars.get(myChar.toLowerCase()) + 1);
						}
						total[0] = total[0] + 1;
					}
				}
				
			}
		// print file
		boolean append = true;
		FileWriter fw = new FileWriter(ROOT_DIR + "charCount.txt", append);
		PrintWriter pw = new PrintWriter(fw, true);
		try {
			
			// iterate through hashmap and print to file
			chars.forEach((key,value) ->  {
				double percentage = ((double)value / (double)total[0]) * 100;
					pw.println(key + ": " + value + " ,  percentage: " + (Math.round(percentage * 100.0) / 100.0) + "%");
				});
		
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			fw.close();
		}
		System.out.println(chars);
		} catch (FileNotFoundException e) {        
			System.out.println("Fitxer no existeix");    
		} catch (IOException e) {        
			System.out.println(e.getMessage());    
		} finally {        
			if (fr != null) fr.close();    
		}
	
	}
}
