package jacint.daw;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Ex8 {

	/*
	 * EXERCICI 8
		Crear un programa que donada una url, agafi tots els links que contingui 
		i els guardi en un fitxer de text. En un  altre fitxer guardarem totes 
		les dades de la pàgina eliminant tots els tags que trobem.  
		Fes servir els Readers / Writers (apartat 1.3. dels apunts) per a llegir 
		i escriure en fitxer
	 */
	
	////////////////////////////////////////
	////////THIS PROGRAM CAPTURES LINKS	///////////
	///////////////////////////////////////
	// EXAMPLE <a href="http://www.google.com">link</a> ===>> http://www.google.com

	public static void main(String[] args) throws IOException {
		String url = "http://www.escoladeltreball.org";
		BufferedReader br = null;
		String ROOT_DIR = "ex8Dir/";

		try {
			URL miUrl = new URL(url);

			InputStreamReader in = new InputStreamReader(miUrl.openStream()); 
			br = new BufferedReader(in);

			String inputLine = "";

			while ( (inputLine = br.readLine() )!= null ){
				if (inputLine.matches(".*href.*")){
					//System.out.println(inputLine);
					// String to be scanned to find the pattern.
				      String line = "asdasdad<a href=\"heythere.com.es/asidjidas?=1\"";
				      String pattern = "(a href=\")([^\"]+)";

				      // Create a Pattern object
				      Pattern r = Pattern.compile(pattern);

				      // Now create matcher object.
				      Matcher m = r.matcher(inputLine);
				      
				      if (m.find( )) {
				         //System.out.println("Link: " + m.group(2) );
				      // print file
				 		boolean append = true;
				 		FileWriter fw = new FileWriter(ROOT_DIR + "linkCollection.txt", append);
				 		PrintWriter pw = new PrintWriter(fw, true);
				 		try {
				 			// print link line to file
				 			pw.println(m.group(2));
				 		
				 		} catch (Exception e) {
				 			System.out.println(e.getMessage());
				 		} finally {
				 			fw.close();
				 		}
				      } 
				}
		
			}


		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			br.close();
		}
	}


}
