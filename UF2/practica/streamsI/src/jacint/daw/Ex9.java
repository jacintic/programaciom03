package jacint.daw;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Ex9 {

	/*
	 * EXERCICI 9
		Crear un programa que donat un directori ens mostri tots els fitxers 
		i directoris que té per sota. Feu un mètode mostrarFitxersDirectoris() 
		que serà el que fiqui en un ArrayList el nom dels arxius i directoris 
		que trobi a més a més aquest mètode haurà de ser recursiu pel cas que 
		dins un ddiirectori trobi un directori.
		Fes servir la classe File (apartat 1.4.2. dels apunts).
	 */

	public static void main(String[] args) throws IOException {
		String ROOT_DIR = "ex9Dir";
		List <String> myFilesList = new ArrayList<String>();
		mostrarFitxersDirectoris(ROOT_DIR,0, myFilesList);
		myFilesList.forEach((file) -> System.out.println(file));
	}

	private static void  mostrarFitxersDirectoris(String dir, int nivell, List <String> myFilesList) {
		File f = new File(dir); 
		File[] listOfFiles = f.listFiles();
		for (File myFile : listOfFiles) {
			String prefix = "";
			for (int i = 0; i < nivell; i++) {
				prefix += "-";
			}
			if (!myFile.isDirectory()) {
				myFilesList.add(prefix + "f: " +  myFile.getName());
			} else {
				myFilesList.add(prefix + "d: " +  myFile.getName());
				mostrarFitxersDirectoris(dir + "/" + myFile.getName() + "/", nivell + 1, myFilesList);
			}
		}
	}

	
	
}


