# instructions to launch PHPmyadmin  
```
Encenem al LAMPP la base de dades MySQL i l'Apache Web Server (l'apache "serveix" aquesta interfície gràfica de gestió de BBDD mysql, ha d'estar engegat).

$ cd /opt/lampp

$ sudo ./manager-linux-x64.run

Anem al browser a localhost/phpmyadmin  -> Pestanya Comptes d' usuari -> root a localhost - > Edita els privilegis -> Canvia la contrasenya de root a localhost.

La canviem per "admin" i salvem al botó "executa".

És preferible crear comptes d' usuaris per a les aplicacions, i no treballar mai amb la de root, això és només un exemple.

Ara en logar-nos, no agafa el nou password.

Anem al fitxer de configuració de phpmyadmin, a canviar-ho també:

Editem config.inc.php , que es troba a : /opt/lampp/phpmyadmin
i posem la nova contrasenya "admin", on per defecte no hi ha res.

Fem restart all al XAMPP i refresquem al navegador el phymyadmin, hauria d'agafar el login de root amb la nova contrasenya.

Recordeu per crear stored procedures: clicar sobre la Base de Dades, per exemple, calendar, i apareix la pestanya "Rutines".

Per tancar el XAMPP, cal tancar tots els serveis prèviament.
```
